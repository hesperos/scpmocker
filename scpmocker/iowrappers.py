#!/usr/bin/python

import os
import shutil
import sys

class IOWrappers(object):
    """
    Set of utility wrappers for file system access
    """

    def rmEntry(self, path):
        """
        Remove either a file or a directory recursively
        """
        try:
            if os.path.isdir(path):
                os.removedirs(path)

            elif os.path.exists(path):
                os.remove(path)

        except OSError:
            pass

    def listDirectories(self, path):
        """
        Return a list of directories under path
        """
        return [ d for d in os.listdir(path)
                if os.path.isdir(os.path.join(path, d)) ]

    def ensureDirectory(self, path):
        """
        Ensure directory exist. Create it if it doesn't
        """
        if not os.path.exists(path):
            os.mkdir(path)
            return True
        return False

    def catFile(self, path: str):
        """
        Copy file to stdout
        """
        try:
            os = sys.stdout
            with open(path, mode='r') as infile:
                shutil.copyfileobj(infile, os)
                os.flush()
        except IOError:
            # Broken PIPE - redirecting to process which doesn't read STDIN
            pass

    def slurpFile(self, path):
        """
        Read whole file
        """
        data = ''
        with open(path) as f:
            data = f.read()
        return data

    def slurpFileLines(self, path):
        data = []
        with open(path) as f:
            data = [ l.strip() for l in f.readlines() ]
        return data

    def copyFile(self, src, dst):
        """
        Copy src file contents to dst
        """
        if os.path.exists(src):
            shutil.copyfile(src, dst)

    def echoFile(self, path, data):
        """
        Output data to file
        """
        with open(path, 'a+') as f:
            f.write(data)

