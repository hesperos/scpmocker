#!/usr/bin/python

import os

class CmdCounters(object):
    """
    Non-volatile counters for command calls

    This class provides a simple API for command call counters
    """

    def __init__(self, db):
        """
        This API operates on persistent dictionary abstractions, db should be
        an implementation of such
        """
        self.countersDb = db

    def __enter__(self):
        """
        Self returning context manager
        """
        return self

    def __exit__(self, extType, excValue, traceback):
        """
        Close the db on context exit
        """
        self.countersDb.close()

    def increment(self, cmd):
        """
        Increment counter for given command
        """
        value = self.get(cmd)
        self.countersDb.set(cmd, str(value + 1))

    def decrement(self, cmd):
        """
        Decrement counter for given command
        """
        value = self.get(cmd)
        if value > 0:
            self.countersDb.set(cmd, str(value - 1))

    def get(self,cmd):
        """
        Simple wrapper for getting values from the DB and initializing them in
        case they don't yet exist.
        """
        try:
            return int(self.countersDb.get(cmd, '0'))
        except TypeError:
            return 0
        except ValueError:
            return 0
