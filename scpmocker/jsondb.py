#!/usr/bin/python2

import json

class JsonDb(object):
    """
    Non-volatile dictionary implementation operating on JSON files
    """
    def __init__(self, dbPath):
        """
        Requires path to the JSON file
        """
        self.dbPath = dbPath
        self.data = {}
        self.sync()

    def __enter__(self):
        """
        Context manager, return self
        """
        return self

    def __exit__(self, *args):
        """
        Call close on context exit
        """
        self.close()

    def sync(self):
        """
        Sync values with the disk - refresh the data with what's on disk
        """
        # make sure the file exists
        open(self.dbPath, 'a+').close()

        data = self.data

        # now, open
        with open(self.dbPath, 'r') as db:
            try:
                data = json.load(db)
                self.data = data
            except ValueError:
                # it's probably empty
                pass

    def flush(self):
        """
        Flush memory dictionary back to disk
        """
        # write JSON
        with open(self.dbPath, 'w') as db:
            json.dump(self.data, db)

    def get(self, key, default = ''):
        """
        Get key value or return default if the key doesn't exist
        """
        return str(self.data.get(key, default))

    def set(self, key, value):
        """
        Set key value
        """
        self.data[key] = str(value)

    def close(self):
        """
        Close the db and flush memory cached values to disk
        """
        self.flush()


