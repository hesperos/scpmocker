#!/usr/bin/python

import argparse
import os
import sys
import tempfile

from . import cmdmock, cmdcounters, jsondb, iowrappers

SCPMOCKSELF = 'scpmocker'
VERSION = "0.2"
REVISION = "$Format:%H$"

def makeParser():
    ap = argparse.ArgumentParser(prog = 'scpmocker',
            description = 'shell command programmable mocker',
            epilog = "version: %s, rev: %s" % (VERSION, REVISION))

    ap.add_argument('-c','--cmd',
            help = 'command to mock',
            required = True)

    subparsers = ap.add_subparsers(help = 'action', dest = 'action')

    statParser = subparsers.add_parser('status')
    progParser = subparsers.add_parser('program')

    statParser.add_argument('-C', '--call-count',
            action = 'store_true',
            help = 'get call count')

    statParser.add_argument('-A', '--call-args',
            help = 'get call arguments')

    progParser.add_argument('-e', '--exit-status',
            help = 'exit status')

    progParser.add_argument('-a', '--always',
            help = 'produce this output for every call',
            action = 'store_true',
            default = False)

    sog = progParser.add_mutually_exclusive_group()

    sog.add_argument('-s', '--string-stdout',
            help = 'string to be produced on stdout')

    sog.add_argument('-f', '--file-stdout',
            help = 'file to be produced on stdout')

    return ap

def programMock(cmdMock, cmdCounters, args):
    with tempfile.NamedTemporaryFile(mode="w+") as tempStdout:
        stdoutPath = args.file_stdout

        if args.string_stdout:
            tempStdout.write(args.string_stdout)
            tempStdout.seek(0)
            stdoutPath = tempStdout.name

        cmdMock.programCall(args.cmd,
                stdoutPath,
                args.exit_status,
                args.always)

def getMockStatus(cmdMock, cmdCounters, args):
    if args.call_count:
        print(cmdCounters.get(args.cmd))

    if args.call_args:
        data = cmdMock.getArgv(args.cmd, str(args.call_args))
        if len(data):
            print(' '.join(data))

def main():
    mock = os.path.basename(sys.argv[0])
    scpDbPath = os.environ.get('SCPMOCKER_DB_PATH', '.')
    counters = 'counters.json'
    rv = 0
    io = iowrappers.IOWrappers()
    io.ensureDirectory(scpDbPath)

    with cmdcounters.CmdCounters(jsondb.JsonDb(
        os.path.join(scpDbPath, counters))) as cc:

        cmd = cmdmock.CmdMock(io,
                cc,
                scpDbPath)

        if mock == SCPMOCKSELF:
            # mock programming
            ap = makeParser()
            args = ap.parse_args()

            try:
                {
                    'status': getMockStatus,
                    'program': programMock,
                }[args.action](cmd, cc, args)
            except KeyError:
                pass

        else:
            # mock execution
            rv = cmd(mock)

    sys.exit(rv)

