#!/usr/bin/python

from setuptools import setup, find_packages

setup(name = 'scpmocker',
    version = '1.0',
    packages = find_packages(),

    tests_require = [
        'mock',
        ],

    install_requires = [
        ],

    entry_points = {
        'console_scripts': [
            'scpmocker = scpmocker.main:main',
            ],
        },

    test_suite = "test",

    author = "Tomasz Wisniewski",
    author_email = "tomasz.wisni3wski@gmail.com",
    description = "shell command programmable parser",
    url = "http://github.com/dagon666/scpmocker",
)
