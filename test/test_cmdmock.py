#!/usr/bin/python

import mock
import unittest
import tempfile
import shutil
import os
import sys
from io import StringIO

from scpmocker import cmdmock, iowrappers, cmdcounters
from test import temporarydirectory


class TestCmdCounters(unittest.TestCase):
    def setUp(self):
        self.mockIo = mock.create_autospec(iowrappers.IOWrappers)
        self.mockCounters = mock.create_autospec(cmdcounters.CmdCounters)

    def test_ifProgramCallCreatesDirectoryForCommand(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            cmdMock = cmdmock.CmdMock(self.mockIo,
                    self.mockCounters, tmp.path)
            cmd = 'echo'
            cmdMock.programCall(cmd)

            self.assertEqual(2, self.mockIo.ensureDirectory.call_count)

            # verify last call
            self.mockIo.ensureDirectory.assert_called_with(
                    os.path.join(tmp.path, cmd, '1'))

    def test_ifProgramCallHandlesIsRepeatedFlag(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            cmdMock = cmdmock.CmdMock(self.mockIo,
                    self.mockCounters, tmp.path)

            for cmd in [ 'echo', 'ls' ]:
                cmdMock.programCall(cmd, None, 0, True)
                self.assertEqual(2,
                        self.mockIo.ensureDirectory.call_count)

                # verify last call
                self.mockIo.ensureDirectory.assert_called_with(
                        os.path.join(tmp.path, cmd, '0'))

                self.mockIo.reset_mock()

    def test_ifCreatesSequenceDirectories(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            cmdMock = cmdmock.CmdMock(self.mockIo,
                    self.mockCounters, tmp.path)

            for cmd in [ 'echo', 'ls' ]:
                dirs = []
                for n in range(16):
                    self.mockIo.listDirectories.return_value = dirs

                    cmdMock.programCall(cmd)

                    dirName = (str(n + 1))
                    dirs.append(dirName)

                    self.assertEqual(2,
                            self.mockIo.ensureDirectory.call_count)

                    # verify last call
                    self.mockIo.ensureDirectory.assert_called_with(
                            os.path.join(tmp.path, cmd, dirName))

                    self.mockIo.reset_mock()

    def test_ifCopiesStdoutFile(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            cmdMock = cmdmock.CmdMock(self.mockIo,
                    self.mockCounters, tmp.path)

            cmd = 'echo'
            sopath = 'path_to_so'
            cmdMock.programCall(cmd, sopath)

            self.mockIo.copyFile.assert_called_once_with(sopath,
                    os.path.join(tmp.path, cmd, '1', 'stdout'))

    def test_ifCreatesExitStatusFile(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            cmdMock = cmdmock.CmdMock(self.mockIo,
                    self.mockCounters, tmp.path)

            cmd = 'echo'
            es = 123
            cmdMock.programCall(cmd, None, es)

            self.mockIo.echoFile.assert_called_once_with(
                    os.path.join(tmp.path, cmd, '1', 'exit_status'),
                    str(es))

    @mock.patch('sys.stdout', new_callable = StringIO)
    def test_ifCallProducesDefaultValues(self, mockStdout):
        with temporarydirectory.TemporaryDirectory() as tmp:
            cmdMock = cmdmock.CmdMock(self.mockIo,
                    self.mockCounters, tmp.path)

            cmd = 'echo'
            rv = cmdMock(cmd)

            self.mockCounters.increment.assert_called_once_with(cmd)
            self.mockCounters.get.assert_called_once_with(cmd)
            self.assertFalse(self.mockIo.slurpFile.called)

            self.assertEqual(0, rv)
            self.assertEqual('', mockStdout.getvalue())

    def test_ifCallReadsTheProgramFiles(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            with mock.patch('os.path') as mockOsPath:
                cmdMock = cmdmock.CmdMock(self.mockIo,
                        self.mockCounters, tmp.path)

                n = '123'
                cmd = 'echo'

                self.mockCounters.get.return_value = n
                mockOsPath.exists.return_value = True
                rv = cmdMock(cmd)

                self.assertTrue(self.mockIo.catFile.called)
                self.assertTrue(self.mockIo.slurpFile.called)

    def test_ifCallProducesDefaultExitStatusOnInvalidProgramData(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            with mock.patch('os.path') as mockOsPath:
                cmdMock = cmdmock.CmdMock(self.mockIo,
                        self.mockCounters, tmp.path)

                n = '123'
                cmd = 'echo'

                self.mockCounters.get.return_value = n
                mockOsPath.exists.return_value = True
                self.mockIo.slurpFile.return_value = 'silly return value'
                rv = cmdMock(cmd)

                self.assertTrue(self.mockIo.slurpFile.called)
                self.assertEqual(0, rv)

    def test_ifCallProducesDesiredExitStatus(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            with mock.patch('os.path') as mockOsPath:
                cmdMock = cmdmock.CmdMock(self.mockIo,
                        self.mockCounters, tmp.path)

                n = '123'
                cmd = 'echo'
                es = 456

                self.mockCounters.get.return_value = n
                mockOsPath.exists.return_value = True
                self.mockIo.slurpFile.return_value = es
                rv = cmdMock(cmd)

                self.assertTrue(self.mockIo.slurpFile.called)
                self.assertEqual(es, rv)

    def test_ifCallCollectsArgv(self):
        with temporarydirectory.TemporaryDirectory() as tmp:
            with mock.patch('os.path.exists') as mockOsPathExists:
                argvs = list("some arguments")
                mockOsPathExists.return_value = True
                with mock.patch.object(sys, 'argv', argvs) as argvMock:
                    cmdMock = cmdmock.CmdMock(self.mockIo,
                            self.mockCounters, tmp.path)

                    n = '123'
                    cmd = 'echo'
                    es = 456

                    self.mockCounters.get.return_value = n
                    rv = cmdMock(cmd)

                    self.mockIo.echoFile.assert_has_calls(
                            [ mock.call(os.path.join(tmp.path, cmd, n, 'args'), a + "\n")
                                for a in argvs[1:] ])

if __name__ == '__main__':
    unittest.main()
