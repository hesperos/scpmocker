#!/usr/bin/python

import json
import os
import tempfile
import unittest

from scpmocker import jsondb

class TmpDbFile(object):
    def __init__(self, path):
        self.path = path
        self.cleanup()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.cleanup()

    def cleanup(self):
        try:
            os.remove(self.path)
        except OSError:
            pass

class TestJsonDb(unittest.TestCase):

    def setUp(self):
        self.exampleData = {
                'key1': 'value1',
                'key2': 123,
                'other': 456,
                }

    def test_ifCreatesNewDbIfItDoesntExist(self):
        dbPath = './dbfile.tmp'
        with TmpDbFile(dbPath):
            self.assertFalse(os.path.exists(dbPath))
            with jsondb.JsonDb(dbPath):
                self.assertTrue(os.path.exists(dbPath))

    def test_ifSyncsAndFlushesTheData(self):
        with tempfile.NamedTemporaryFile(mode="w+") as tmpf:
            json.dump(self.exampleData, tmpf)
            tmpf.seek(0)

            with jsondb.JsonDb(tmpf.name):
                pass

            tmpf.seek(0)
            readBack = json.load(tmpf)
            self.assertEqual(self.exampleData, readBack)

    def test_ifAddsNewData(self):
        newKey = 'new key'
        newValue = 'value for new key'

        with tempfile.NamedTemporaryFile(mode="w+") as tmpf:
            json.dump(self.exampleData, tmpf)
            tmpf.seek(0)

            with jsondb.JsonDb(tmpf.name) as jdb:
                jdb.set(newKey, newValue)

            tmpf.seek(0)
            readBack = json.load(tmpf)

            # check original keys
            for k, v in self.exampleData.items():
                self.assertTrue(k in readBack)
                self.assertEqual(readBack[k], v)

            self.assertTrue(newKey in readBack)
            self.assertEqual(readBack[newKey], newValue)

    def test_ifModifiesData(self):
        key = 'key1'
        newValue = 'new value for new key1'

        with tempfile.NamedTemporaryFile(mode="w+") as tmpf:
            json.dump(self.exampleData, tmpf)
            tmpf.seek(0)

            with jsondb.JsonDb(tmpf.name) as jdb:
                jdb.set(key, newValue)

            tmpf.seek(0)
            readBack = json.load(tmpf)

            self.assertTrue(key in readBack)
            self.assertNotEqual(readBack[key], self.exampleData[key])
            self.assertEqual(readBack[key], newValue)


if __name__ == '__main__':
    unittest.main()

