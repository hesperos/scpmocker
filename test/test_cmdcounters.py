#!/usr/bin/python

import os
import unittest
import mock

from scpmocker import cmdcounters, jsondb

class TestCmdCounters(unittest.TestCase):
    def setUp(self):
        self.mockJdb = mock.create_autospec(jsondb.JsonDb)
        self.cc = cmdcounters.CmdCounters(self.mockJdb)

    def test_ifDefaultValueForGetIsString(self):
        key = 'some key'
        self.mockJdb.get.return_value = '123'
        self.cc.get(key)
        self.mockJdb.get.assert_called_with(key, '0')

    def test_ifConvertsStringsToInteger(self):
        key = 'some key'
        self.mockJdb.get.return_value = '123'
        rv = self.cc.get(key)
        self.assertEqual(123, rv)
        self.assertTrue(isinstance(rv, int))

    def test_ifReturnsEmptyStringIfDbReturnsNonInt(self):
        key = 'some key'
        self.mockJdb.get.return_value = 'non integer'
        rv = self.cc.get(key)
        self.assertEqual(0, rv)

    def test_ifReturnsEmptyStringIfDbReturnsNone(self):
        key = 'some key'
        self.mockJdb.get.return_value = None
        rv = self.cc.get(key)
        self.assertEqual(0, rv)

    def test_ifIncrementCallsSetWithStringValue(self):
        key = 'some key'
        self.mockJdb.get.return_value = '123'
        self.cc.increment(key)
        self.mockJdb.get.assert_called_with(key, '0')
        self.mockJdb.set.assert_called_with(key, '124')

    def test_ifDecrementCallsSetWithStringValue(self):
        key = 'some key'
        self.mockJdb.get.return_value = '123'
        self.cc.decrement(key)
        self.mockJdb.get.assert_called_with(key, '0')
        self.mockJdb.set.assert_called_with(key, '122')

    def test_ifDecrementDoesntGoBelowZero(self):
        key = 'some key'
        self.mockJdb.get.return_value = '0'
        self.cc.decrement(key)
        self.mockJdb.get.assert_called_with(key, '0')
        self.assertEqual(0, self.mockJdb.set.call_count)

if __name__ == '__main__':
    unittest.main()
