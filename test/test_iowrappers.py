#!/usr/bin/python

import mock
import os
import tempfile
import unittest
import uuid

from io import BytesIO

from scpmocker import iowrappers
from test import temporarydirectory

class TestIoWrappers(unittest.TestCase):

    def setUp(self):
        self.io = iowrappers.IOWrappers()

    @mock.patch('os.removedirs')
    @mock.patch('os.path')
    def test_rmEntryCallsRemoveDirsForDirectories(self,
            osPathMock, osRemoveDirsMock):
        path = 'some path'
        osPathMock.isdir.return_value = True
        self.io.rmEntry(path)
        osRemoveDirsMock.assert_called_once_with(path)

    @mock.patch('os.remove')
    @mock.patch('os.path')
    def test_rmEntryCallsRemoveForFiles(self,
            osPathMock, osRemoveMock):
        path = '/some/path'
        osPathMock.isdir.return_value = False
        self.io.rmEntry(path)
        osRemoveMock.assert_called_once_with(path)


    @mock.patch('os.path.isdir')
    @mock.patch('os.listdir')
    def test_listDirectoriesReturnsOnlyDirectoryList(self,
            osListDirMock, osIsDirMock):

        path = '/some/path'
        rv = { 'a': True, 'b': False, 'c': False, 'd': True, 'e': True }

        dirsOnly = [ k for k,v in rv.items() if v ]

        osListDirMock.return_value = rv.keys()
        osIsDirMock.side_effect = rv.values()
        dirs = self.io.listDirectories(path)
        self.assertEqual(dirsOnly, dirs)

    @mock.patch('os.path')
    @mock.patch('os.mkdir')
    def test_ensureDirectoryDoesntDoAnythingIfDirExists(self,
            osMkDirMock, osPathMock):
        path = '/some/path'
        osPathMock.exists.return_value = True
        osMkDirMock.assert_not_called()
        self.assertFalse(self.io.ensureDirectory(path))

    @mock.patch('os.path')
    @mock.patch('os.mkdir')
    def test_ensureDirectoryDoesCreateDirIfDoesntExist(self,
            osMkDirMock, osPathMock):
        path = '/some/path'
        osPathMock.exists.return_value = False
        self.assertTrue(self.io.ensureDirectory(path))
        osMkDirMock.assert_called_once_with(path)

    @mock.patch('sys.stdout', new_callable = BytesIO)
    def test_catFilePrintsToStdOut(self, mockStdout):
        msg = "To be printed to stdout"
        msg = msg.encode()
        with tempfile.NamedTemporaryFile() as tmpf:
            tmpf.write(msg)
            tmpf.seek(0)
            self.io.catFile(tmpf.name)

        self.assertEqual(msg, mockStdout.getvalue())

    def test_slurpFileReadWholeFile(self):
        msg = "some message"
        with tempfile.NamedTemporaryFile() as tmpf:
            tmpf.write(msg.encode())
            tmpf.seek(0)
            self.assertEqual(msg, self.io.slurpFile(tmpf.name))

    def test_copyFileCopiesFiles(self):
        with tempfile.NamedTemporaryFile() as src:
            msg = "test message"
            src.write(msg.encode())
            src.seek(0)

            with tempfile.NamedTemporaryFile() as dst:
                self.io.copyFile(src.name, dst.name)
                readMsg = dst.read().decode()
                self.assertEqual(msg, readMsg)

    def test_echoFileOutputsToFile(self):
        with tempfile.NamedTemporaryFile() as src:
            msg = "test message"
            self.io.echoFile(src.name, msg)
            src.seek(0)
            readMsg = src.read().decode()
            self.assertEqual(msg, readMsg)

    def test_echoFileCreatesFileIfDoesntExist(self):
        with temporarydirectory.TemporaryDirectory() as tmpd:
            filename = uuid.uuid4().hex
            filepath = os.path.join(tmpd.path, filename)

            self.assertFalse(os.path.exists(filepath))
            msg = "test message"
            self.io.echoFile(filepath, msg)
            self.assertTrue(os.path.exists(filepath))

    def test_echoFileAppendsToExistingFile(self):
        with tempfile.NamedTemporaryFile() as src:
            msgs = [ "test message", "msg2" ]

            for m in msgs:
                m = m + "\n"
                self.io.echoFile(src.name, m)

            src.seek(0)
            readMsg = [ l.decode().strip() for l in src.readlines() ]
            self.assertEqual(msgs, readMsg)

