[![Build Status](https://travis-ci.org/dagon666/scpmocker.svg?branch=master)](https://travis-ci.org/dagon666/scpmocker)

# Simple Command Programmable Mocker

*scpmocker* is a small tool which can be used to
shadow a real system command with a programmable mock.
The mock's stdout and exit status can be
pre-programmed for any set of command line arguments.

The script is meant to be used in bash unit tests to replace real system
commands and easily verify script's behaviour with different output from
commands it uses internally.

## Mock's Database

The mock reads the contents of a "database" directory to determine
the stdout and exit status that should be produced for a given
command call. Each command's stdout (`stdout`) and exit status
(`exit_status`) files should be placed in the database under
directory `command/[0-9]+`. The directories define output for each
command call separately. Should the same output for a command be
produced every time, a directory `command/0` should be created.

Example database's directory tree:

```
.
├── counters.db # (scpmocker's invocation database)
├── echo
│   └── 1
│       ├── exit_status # (exit status for first call to echo)
│       └── stdout # (stdout for first call to echo
├── ls
│   └── 0
│       ├── exit_status # (exit status for all calls to ls)
│       └── stdout # (stdout for all calls to ls)
├── pwd # (no exit status and stdout programmed - defaults will be used)
└── true # (only exit status is programmed for calls to true)
    ├── 1
    │   └── exit_status
    ├── 2
    │   └── exit_status
    └── 3
        └── exit_status
```

This tree can be created manually or can be programmed using mocks
CLI interface.

## Programming the mock

1. Set the environment variable `SCPMOCKER_DB_PATH` to the location
of the database's root. You can create the database directory tree manually
creating files for each call or use the mocks CLI:

    $ SCPMOCKER_DB_PATH=./mockdb scpmocker -c echo program -f somefile.txt -e 123
    $ SCPMOCKER_DB_PATH=./mockdb scpmocker -c echo program -s "example echo output"
    $ SCPMOCKER_DB_PATH=./mockdb scpmocker -c echo program -e 100
    $ SCPMOCKER_DB_PATH=./mockdb scpmocker -c echo program
    $ SCPMOCKER_DB_PATH=./mockdb scpmocker -c ls program -e 1

This will produce the following tree

```
mockdb/
├── counters.json
├── echo
│   ├── 1
│   │   ├── exit_status
│   │   └── stdout
│   ├── 2
│   │   └── stdout
│   ├── 3
│   │   └── exit_status
│   └── 4
└── ls
    └── 1
            └── exit_status

```

## Using the mock

Assuming it has been programmed as above

    ln -sf path_to_scpmocker echo
    ln -sf path_to_scpmocker ls

    $ SCPMOCKER_DB_PATH=./mockdb ./echo
    contents of somefile.txt
    ...
    $ echo $?
    123

    $ SCPMOCKER_DB_PATH=./mockdb ./echo
    example echo output
    $ echo $?
    0

    $ SCPMOCKER_DB_PATH=./mockdb ./echo
    $ echo $?
    100

    $ SCPMOCKER_DB_PATH=./mockdb ./echo
    $ echo $?
    0

## Dependencies

All of mentioned below should be available for Python 2.6+.

- argparse
- json
- mock (tests)
